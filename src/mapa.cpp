#include "batalhanaval.hpp"
using namespace std;

Mapa::Mapa(){

  }
Mapa::~Mapa(){
}
int Mapa::lugar_ocupado(int x, int y){
  for(int i=0 ; i<13 ; i++){
    for(int j=0 ; j<13 ; j++){
      if(matriz[x][y]==-1){
        //cout << "Lugar ocupado" << endl;
        return 1;
      }
      if(matriz[i][j]!=-1 && i==x && y==j){
        matriz[x][y]=-1;
        return 0;
      }
    }
  }
  return 2;
}
void Mapa::setcanoa(int c, int x1, int y1){
  if((x1 >= 0 && x1 < 13) && (y1 >= 0 && y1 < 13)) {
  if(lugar_ocupado(y1,x1)==0){
    coordenadas_canoa[x1][y1] = canoa;
    coordenadas_canoa[x1][y1].set_numero(-5);
    coordenadas_canoa[x1][y1].setcanoanumero(c);
    //cout << "Setcanoa de nas coordenadas : " << y1 << "  " << x1 << endl;
}
}
}
void Mapa::setsubmarino(int x, int x1, int y1, int x2, int y2){
  if((x1 >= 0 && x1 < 13) && (x2 >= 0 && x2 < 13) && (y1 >= 0 && y1 < 13) && (y2 >= 0 && y2 < 13)) {
  if(lugar_ocupado(y1, x1)==0 && lugar_ocupado(y2,x2)==0){
  coordenadas_submarino[x1][y1] = submarino;
  coordenadas_submarino[x1][y1].set_numero(-5);
  coordenadas_submarino[x1][y1].setsubmarinonumero(x);
  //cout << "Setsubmarino nas coordenadas : " << y1 << "  " << x1  << "  " << y2 << " " << x2 << endl;
  coordenadas_submarino[x2][y2] = submarino;
  coordenadas_submarino[x2][y2].set_numero(-5);
  coordenadas_submarino[x2][y2].setsubmarinonumero(x);
  //cout << "Setsubmarino nas coordenadas : " << y1 << "  " << x1  << "  " << y2 << " " << x2 << endl;
}
}
}
void Mapa::setportaavioes(int a,int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4){
if((x1 >= 0 && x1 < 13) && (x2 >= 0 && x2 < 13) && (y1 >= 0 && y1 < 13) && (y2 >= 0 && y2 < 13) && (x3 >= 0 && x3 < 13) && (x4 >= 0 && x4 < 13) & (y3 >= 0 && y3 < 13) && (y4 >= 0 && y4 < 13)) {
  if(lugar_ocupado(y1, x1)==0 && lugar_ocupado(y2,x2)==0 && lugar_ocupado(y3,x3)==0 && lugar_ocupado(y4,x4)==0){
    coordenadas_aviao[x1][y1] = aviao;
    coordenadas_aviao[x1][y1].set_numero(-5);
    coordenadas_aviao[x1][y1].setaviaonumero(a);
    //cout << "Set porta-avião " << y1 << "  " << x1  << endl;
    coordenadas_aviao[x2][y2] = aviao;
    coordenadas_aviao[x2][y2].set_numero(-5);
    coordenadas_aviao[x2][y2].setaviaonumero(a);
    //cout << "Set porta-avião " << y2 << " " << x2  << endl;
    coordenadas_aviao[x3][y3] = aviao;
    coordenadas_aviao[x3][y3].set_numero(-5);
    coordenadas_aviao[x3][y3].setaviaonumero(a);
    //cout << "Set porta-avião " << y3 << "  " << x3 << endl;
    coordenadas_aviao[x4][y4] = aviao;
    coordenadas_aviao[x4][y4].set_numero(-5);
    coordenadas_aviao[x4][y4].setaviaonumero(a);
    //cout << "Set porta-avião " << y4 << " " << x4 << endl;
  }
}
}
void Mapa::atacar(int x1, int y1){
  for(int i=0; i<13 ; i++){
    for( int j=0 ; j<13 ; j++){
      if( coordenadas_submarino[y1][x1].get_numero()==-2 && x1==j && y1 == i && coordenadas_submarino[y1][x1].get_vida()==0){
        //cout << "Esse submarino já foi afundado" <<endl;
      }
      if( coordenadas_canoa[y1][x1].get_numero()==-2 && x1==j && y1 == i){
        //cout << "Essa canoa já foi afundada" << endl;
      }
      if( coordenadas_aviao[y1][x1].get_numero()==-2 && x1==j && y1 == i && coordenadas_aviao[y1][x1].get_vida()==0){
        //cout << "Esse porta-avião já foi afundado" << endl;
      }
      if( coordenadas_canoa[y1][x1].get_numero()==-5 && x1==j && y1 == i){
        coordenadas_canoa[y1][x1].atacar();
        coordenadas_canoa[y1][x1].afundar();
        coordenadas_canoa[y1][x1].set_numero(-2);
      }
      if( coordenadas_submarino[y1][x1].get_numero()==-3 && x1==j && y1 == i){
        //cout << "Atacando o submarino pela 2º vez: "<<   coordenadas_submarino[i][j].getsubmarinonumero() <<" nas coordenadas: x1= "<< i << " y1=" << j << endl;
        coordenadas_submarino[y1][x1].atacar();
        coordenadas_submarino[y1][x1].set_numero(-2);
        coordenadas_submarino[y1][x1].set_vida(0);
        coordenadas_submarino[y1][x1].afundar();
      }
      if( coordenadas_submarino[y1][x1].get_numero()==-5 && x1==j && y1 == i && coordenadas_submarino[y1][x1].get_vida()==2){
        //cout << "ATACANDO SUBMARINO: "<<   coordenadas_submarino[y1][x1].getsubmarinonumero() <<" nas coordenadas: x1= "<< i << " y1=" << j << endl;
        coordenadas_submarino[y1][x1].atacar();
        coordenadas_submarino[y1][x1].set_numero(-3);
        coordenadas_submarino[y1][x1].set_vida(1);
        //cout << "VIDA DO SUBMARINO " << coordenadas_submarino[y1][x1].get_vida() << endl;
      }

      if( (coordenadas_aviao[y1][x1].get_numero()==-5 || coordenadas_aviao[y1][x1].get_numero()==-3) && x1==j && y1 == i && coordenadas_aviao[y1][x1].get_vida()==1){
        //cout << "ATACANDO PORTA AVIAO de número  "<<  coordenadas_aviao[y1][x1].getaviaonumero() << endl;
        coordenadas_aviao[y1][x1].atacar();
        coordenadas_aviao[y1][x1].set_vida(0);
        coordenadas_aviao[y1][x1].habilidade();
        coordenadas_aviao[y1][x1].set_numero(-3);
        if(coordenadas_aviao[y1][x1].get_vida()== 0){
          coordenadas_aviao[y1][x1].set_numero(-2);
          coordenadas_aviao[y1][x1].afundar();
        }
      }
      if( ( coordenadas_aviao[y1][x1].get_numero()==0 ||  coordenadas_aviao[y1][x1].get_numero()==-1) && (coordenadas_canoa[y1][x1].get_numero()==0  || coordenadas_canoa[y1][x1].get_numero()==-1) && (coordenadas_submarino[y1][x1].get_numero()==0 || coordenadas_submarino[y1][x1].get_numero()==-1)  && x1==j && y1 == i){
        cout<<"Você errou o tiro e acertou a água" << endl;
        coordenadas_submarino[y1][x1].set_numero(-1);
        coordenadas_canoa[y1][x1].set_numero(-1);
        coordenadas_aviao[y1][x1].set_numero(-1);
      }
    }
  }
}
void Mapa::printarcampo(){
for(int i=0 ; i<13 ; i++){
    if(i==1){ cout << "0 | ";}
    if(i!=0) {cout << i << "  | ";}
    else{cout << i << "  |  ";}
  }

  cout << endl;
  for(int i=0; i<13 ; i++){
    if(i>9){cout << i << "-| " ;}
    else{cout << i << "- | " ;}
    int valor=0;
    for(int j=0; j<13; j++){
      if(j>=10){          //alinhar o desenho do mapa
        cout << " ";
      }
      if(coordenadas_canoa[j][i].get_numero() == -1 || coordenadas_aviao[j][i].get_numero() == -1 || coordenadas_submarino[j][i].get_numero() == -1){
          cout << "x" << "  | " ;         //Acertou a água "x"
        }
      if(coordenadas_canoa[j][i].get_numero() == -2){
          cout << "*" << "  | " ;         //barco atingido "*"
        }
        if( (coordenadas_canoa[j][i].get_numero() == 0 || coordenadas_canoa[j][i].get_numero() == -5) && (coordenadas_submarino[j][i].get_numero() == 0 || coordenadas_submarino[j][i].get_numero() == -5) && ( coordenadas_aviao[j][i].get_numero() == 0|| coordenadas_aviao[j][i].get_numero() == -5) ){
          cout << "o" << "  | " ;
        }
        if(coordenadas_submarino[j][i].get_numero() == -3){
          cout << "S" << "  | " ;         //barco atingido "x"
          valor++;
        }
        if(coordenadas_submarino[j][i].get_numero() == -2){
          cout << "*" << "  | " ;         //barco atingido "*"
        }
        if(coordenadas_aviao[j][i].get_numero() == -3){
          cout << "P" << "  | " ;         //mostra que desviou o tiro, mas agora sabe onde está o porta-avião
        }
        if(coordenadas_aviao[j][i].get_numero() == -2){
          cout << "*" << "  | " ;         //barco atingido "*"
        }

      }
    cout  << endl;
}
}
void Mapa::barcosrestantes(){
  int a = 0;
  int s = 0;
  int c = 0;
  for(int i=0 ; i<13 ; i++){
    for(int j=0; j<13 ; j++){
      if(coordenadas_canoa[j][i].get_numero() == -5){
        c++;
      }

      if(coordenadas_submarino[j][i].get_numero() == -5){
        s++;
      }

      if(coordenadas_aviao[j][i].get_numero() == -5){
        a++;
      }
    }
  }
  cout<<"canoa: "<< c << endl <<"porta-aviões completos:  " << a/4 << endl <<  "submarinos completos: " << s/2 <<  endl << endl;
}
void Mapa::pontuar(){
  int valor = 0;
  pontuacao.set_pontos(valor);
  for(int i=0; i<13 ; i++){
    for(int j=0; j<13; j++){
      if(coordenadas_canoa[j][i].get_numero() == -2){
        valor++;
        pontuacao.set_pontos(valor);
      }
      if(coordenadas_submarino[j][i].get_numero() == -2){
        valor++;
        pontuacao.set_pontos(valor);
      }
      if(coordenadas_aviao[j][i].get_numero() == -2){
        valor++;
        pontuacao.set_pontos(valor);
      }
      if(coordenadas_aviao[j][i].get_numero() == -1){
      
      }
    }
  }
  cout << " pontuou " << pontuacao.get_pontos() ;
}
int Mapa::checar_vitoria(){
  int valor=0;
  pontuacao.set_pontos(valor);
  for(int i=0; i<13 ; i++){
    for(int j=0; j<13; j++){
      if(coordenadas_canoa[j][i].get_numero() == -5){
        valor++;
        pontuacao.set_pontos(valor);
      }
      if(coordenadas_submarino[j][i].get_numero() == -5 || coordenadas_submarino[j][i].get_numero() == -3){
        valor++;
        pontuacao.set_pontos(valor);
      }
      if(coordenadas_aviao[j][i].get_numero() == -5 || coordenadas_aviao[j][i].get_numero() == -3){
        valor++;
        pontuacao.set_pontos(valor);
      }
    }
  }
  return pontuacao.get_pontos();

}
void Mapa::randomizar(){
  srand( static_cast<unsigned int>(time(NULL)));
  int i=0;
while(i<13){
  int x1 = 0,
  x2 = 0,
  x3 = 0,
  x4 = 0,
  y1 = 0,
  y2 = 0,
  y3 = 0,
  y4 = 0,
  a = 0,
  x = 0,
  c = 0;
  x1 = rand() % 13; // gera um número entre 0 e 13
  y1 = rand() % 13;

  x = rand() % 5;
  if(rand() % 2 == 0){					//cima ou baixo
    //cima
    if(rand() % 2 == 1) {
      y2 = y1 - 1;
      y3 = y2 - 1;
      y4 = y3 - 1;
      x2 = x1;
      x3 = x2;
      x4 = x3;

    }
    //baixo
    else {
      y2 = y1 + 1;
      y3 = y2 + 1;
      y4 = y3 + 1;
      x2 = x1;
      x3 = x2;
      x4 = x3;

    }
  }
  else{         //lado esquerdo ou direito
    //direito
    if(rand() % 2 == 1){
      x2 = x1 + 1;
      x3 = x2 + 1;
      x4 = x3 + 1;
      y2 = y1;
      y3 = y1;
      y4 = y1;

    }
    //esquerdo
  else {
      x2 = x1 - 1;
      x3 = x2 - 1;
      x4 = x3 - 1;
      y2 = y1;
      y2 = y1;
      y3 = y2;
      y4 = y3;
  }
  }
  if((x1 >= 0 && x1 < 13) && (x2 >= 0 && x2 < 13) && (y1 >= 0 && y1 < 13) && (y2 >= 0 && y2 < 13) && (x3 >= 0 && x3 < 13) && (x4 >= 0 && x4 < 13) & (y3 >= 0 && y3 < 13) && (y4 >= 0 && y4 < 13)) {

    if( (x==0 || x==3) && lugar_ocupado(y1, x1)==0){
      c++;
      i++;
       setcanoa(c,y1,x1);
      //  cout << "valor de i " << i << endl;
      // cout << "Colocando canoa randomicamente em x1, y1: " << x1 << "  "<< y1 << endl;
   }
    else if( (x==1 || x>3) && (lugar_ocupado(y1, x1)==0 || lugar_ocupado(y2,x2)==0) ){
      x++;
      i++;
      setsubmarino(x , y1, x1,y2, x2);
      //cout << "valor de i " << i << endl;
    //  cout << "Colocando submarino randomicamente em x1, y1, x2, y2: " << x1 << "  "<< y1 << "  " << x2 << "  "<< y2 <<endl;
  }
    else if(x==2 || lugar_ocupado(y1, x1)==0 || lugar_ocupado(y2,x2)==0 || lugar_ocupado(y3, x3)==0 || lugar_ocupado(y4,x4)==0){
      a++;
      i++;
      setportaavioes(a, y1, x1, y2, x2, y3, x3, y4, x4);
        //cout << "valor de i " << i << endl;
      //cout << "Colocando porta-aviao randomicamente em x1, y1, x2, y2, x3, y3, x4, y4 : " << x1 << "  "<< y1 << "  " << x2 << "  "<< y2 << "  " << x3<< "  " << y3 << "  "<< x4 << "   " << y4 << endl;
    }
  }
}
}
void Mapa::ataque_randomizado(){
  srand(static_cast<unsigned int>(time(NULL)));
  int x1=0;
  int y1=0;
  x1 = rand() % 13; // gera um número entre 0 e 13
  y1 = rand() % 13;
  atacar(x1, y1);
  cout << "CPU atacou em x1 e y1 " << y1 << "  " << x1 << endl;

}
void Mapa::estrategia(){
  srand(static_cast<unsigned int>(time(NULL)));
  for(int i=0; i<13 ; i++){
    for(int j=0; j<13; j++){
      //Atacar denovo o que já foi atacado uma vez
      if(coordenadas_submarino[j][i].get_numero() == -3){
        atacar(i, j);
        cout << "CPU atacou em j e i " << i << "  " << j << endl;
        return;
      }
      //Atacar denovo o que já foi atacado uma vez
      if(coordenadas_aviao[j][i].get_numero() == -3){
        atacar(i, j);
        cout << "CPU atacou em j e i " << i << "  " << j << endl;
        return;
      }
      // Se destruiu um barco tentar acertar os que estão perto
      if(coordenadas_submarino[j][i].get_numero() == -2){
        if(coordenadas_submarino[j+1][i].get_numero() == -5){
        atacar(i, j+1);
        cout << "CPU atacou em j e i " << i << "  " << j+1 << endl;
        return;
        }
        if(coordenadas_submarino[j][i+1].get_numero() == -5){
        atacar(i+1, j);
        cout << "CPU atacou em j e i " << i+1 << "  " << j << endl;
        return;
        }
        if(coordenadas_submarino[j][i-1].get_numero() == -5){
        atacar(i-1, j);
        cout << "CPU atacou em j e i " << i-1 << "  " << j << endl;
        return;
        }
        if(coordenadas_submarino[j-1][i].get_numero() == -5 ){
        atacar(i, j-1);
        cout << "CPU atacou em j e i " << i << "  " << j-1 << endl;
        return;
        }
      }
      if(coordenadas_aviao[j][i].get_numero() == -2){
        if(coordenadas_aviao[j+1][i].get_numero() == -5){
        atacar(i, j+1);
        cout << "CPU atacou em j e i " << i << "  " << j+1 << endl;
        return;
        }
        if(coordenadas_aviao[j][i+1].get_numero() == -5){
        atacar(i+1, j);
        cout << "CPU atacou em j e i " << i+1 << "  " << j << endl;
        return;
        }
        if(coordenadas_aviao[j][i-1].get_numero() == -5){
        atacar(i-1, j);
        cout << "CPU atacou em j e i " << i+1 << "  " << j << endl;
        return;
        }
        if(coordenadas_aviao[j-1][i].get_numero() == -5 ){
        atacar(i, j-1);
        cout << "CPU atacou em j e i " << i << "  " << j-1 << endl;
        return;
        }
      }
  }
}
  int x1=0;
  int y1=0;
  x1 = rand() % 13; // gera um número entre 0 e 13
  y1 = rand() % 13;
  atacar(x1, y1);
  cout << "Cpu atacou em y1 e x1 " << x1 << "  " << y1 << endl;
  cout << "CPU atacou" << endl;
}
void Mapa::estrategiafinal(){
  srand(static_cast<unsigned int>(time(NULL)));
  for(int i=0; i<13 ; i++){
    for(int j=0; j<13; j++){
      // Se destruiu um barco tentar acertar os que estão perto
      if(coordenadas_submarino[j][i].get_numero() == -2){
        if(coordenadas_submarino[j+1][i].get_numero() == -5){
        atacar(i, j+1);
        cout << "GLaDOS atacou em j e i " << i << "  " << j+1 << endl;
        return;
        }
        if(coordenadas_submarino[j][i+1].get_numero() == -5){
        atacar(i+1, j);
        cout << "GLaDOS atacou em j e i " << i+1 << "  " << j << endl;
        return;
        }
        if(coordenadas_submarino[j][i-1].get_numero() == -5){
        atacar(i-1, j);
        cout << "GLaDOS atacou em j e i " << i-1 << "  " << j << endl;
        return;
        }
        if(coordenadas_submarino[j-1][i].get_numero() == -5 ){
        atacar(i, j-1);
        cout << "GLaDOS atacou em j e i " << i << "  " << j-1 << endl;
        return;
        }
      }
      if(coordenadas_aviao[j][i].get_numero() == -2){
        if(coordenadas_aviao[j+1][i].get_numero() == -5){
        atacar(i, j+1);
        cout << "GLaDOS atacou em j e i " << i << "  " << j+1 << endl;
        return;
        }
        if(coordenadas_aviao[j][i+1].get_numero() == -5){
        atacar(i+1, j);
        cout << "GLaDOS atacou em j e i " << i+1 << "  " << j << endl;
        return;
        }
        if(coordenadas_aviao[j][i-1].get_numero() == -5){
        atacar(i-1, j);
        cout << "GLaDOS atacou em j e i " << i+1 << "  " << j << endl;
        return;
        }
        if(coordenadas_aviao[j-1][i].get_numero() == -5 ){
        atacar(i, j-1);
        cout << "GLaDOS atacou em j e i " << i << "  " << j-1 << endl;
        return;
        }
      }
      //Atacar denovo o que já foi atacado uma vez
      if(coordenadas_submarino[j][i].get_numero() == -5){
        atacar(i, j);
        //cout << "lados Cpu atacou em j e i " << i << "  " << j << endl;
        return;
      }
      if(coordenadas_submarino[j][i].get_numero() == -3){
        atacar(i, j);
        cout << "GLaDOS atacou em j e i " << i << "  " << j << endl;
        return;
      }
      //Atacar denovo o que já foi atacado uma vez
      if(coordenadas_aviao[j][i].get_numero() == -5){
        atacar(i, j);
        cout << "GLaDOS atacou em j e i " << i << "  " << j << endl;
        return;
      }
      if(coordenadas_aviao[j][i].get_numero() == -3){
        atacar(i, j);
        cout << "GLaDOS atacou em j e i " << i << "  " << j << endl;
        return;
      }
      if(coordenadas_canoa[j][i].get_numero() == -5){
        atacar(i, j);
        cout << "GLaDOS atacou em j e i " << i << "  " << j << endl;
        return;
      }
  }
}
  int x1=0;
  int y1=0;
  x1 = rand() % 13; // gera um número entre 0 e 13
  y1 = rand() % 13;
  atacar(x1, y1);
  cout << "Cpu atacou em y1 e x1 " << x1 << "  " << y1 << endl;
  cout << "CPU atacou" << endl;
}
