#include "batalhanaval.hpp"


Jogador::Jogador(){
    nome = "";
    nacionalidade = "";
}
Jogador::~Jogador(){
}

string Jogador::get_nome(){
  return nome;
}
void Jogador::set_nome(string nome){
  this->nome = nome;
}
string Jogador::get_nacionalidade(){
  return nacionalidade;
}
void Jogador::set_nacionalidade(string nacionalidade){
  this->nacionalidade = nacionalidade;
}
void Jogador::apresentar(){
    cout << "Nome: " << get_nome() << endl;
    cout << "Nacionalidade: " << get_nacionalidade() << endl;
}
