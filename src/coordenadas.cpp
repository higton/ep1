#include "batalhanaval.hpp"

Coordenadas::Coordenadas(){
}
Coordenadas::~Coordenadas(){
}
void Coordenadas::estrategiafinal(){
  p1.getMapa().estrategiafinal();
}
void Coordenadas::estrategia(){
  p1.getMapa().estrategia();
}
void Coordenadas::ataque_randomizado(){
  p1.getMapa().ataque_randomizado();
}

void Coordenadas::barcosrestantesp1(){
  p1.getMapa().barcosrestantes();
}
void Coordenadas::barcosrestantesp2(){
  p2.getMapa().barcosrestantes();
}
void Coordenadas::printarcampo_p1(){
  p1.getMapa().printarcampo();
}
void Coordenadas::printarcampo_p2(){
  p2.getMapa().printarcampo();
}
void Coordenadas::atacarbarcos_p1(int x1, int y1){
  p1.getMapa().atacar(x1, y1);
}
void Coordenadas::atacarbarcos_p2(int x1, int y1){
  p2.getMapa().atacar(x1, y1);
}
void Coordenadas::pontuarp1(){
  p1.getMapa().pontuar();
}
void Coordenadas::pontuarp2(){
  p2.getMapa().pontuar();
}
int Coordenadas::checar_vitoriap1(){
  p1.getMapa().checar_vitoria();
}
int Coordenadas::checar_vitoriap2(){
  p2.getMapa().checar_vitoria();
}
void Coordenadas::randomizarp1(){
  p1.getMapa().randomizar();
}
void Coordenadas::randomizarp2(){
  p2.getMapa().randomizar();
}
int Coordenadas::get_numlinhas(){
  ifstream file("doc/map_1.txt");
  int i=0;
    string str="";
    while (std::getline(file, str))
    {
      if(int(str[0]) != 0 && int(str[0]) != 35){
      i++;
      }
    }
  numlinhas = i;
  return numlinhas;
}
void Coordenadas::set_numlinhas(int numlinhas){
  this->numlinhas = numlinhas;
}


void Coordenadas::colocarbarcos(int n){
  int submarino = 0,
      canoa = 0,
      aviao = 0,
      valor = 0,
      baixo =0,
      cima = 0,
      esquerda = 0,
      direita = 0,
      c = 0,
      a=0,
      x=0,
      i=0,
      x1 = 0,
      x2 = 0,
      y1 = 0,
      y2 = 0,
      x3 = 0,
      x4 = 0,
      y3 = 0,
      y4 = 0,
      numlinhas = 0;
  string line;
  string content;
  ifstream myfile("doc/map_1.txt");
  if (myfile.is_open())
  {

    while (! myfile.eof() )   // loop lendo linha por linha
    {
      getline (myfile,line);

      // Comparando valores da string do texto
      int pos = 0;      // a cada linha o valor de pos vai zerar
      string str = "";
      while(true && int(line[0]) != 35){     // ignora quando começa com "#"
      numlinhas = get_numlinhas();
        aviao =  line.find("porta-avioes", ++pos);  // valor do aviao quando nao acha porta-avioes é -1
        canoa =  line.find("canoa", ++pos);
        submarino =  line.find("submarino", ++pos);
        baixo = line.find("baixo", ++pos);
        cima = line.find("cima", ++pos);
        esquerda = line.find("esquerda", ++pos);
        direita = line.find("direita", ++pos);
          std::string b = line.substr(0, 2); // separar a parte que eu quero da string
          if( int(line[0])!=0 && int(line[0])!=32){
          std::string s = line.substr(2, 5);
          x1 = atoi(s.c_str());    // "think"
          y1 = atoi(b.c_str());         // Transforma a string das coordenadas em um número inteiro
        }
          if ( direita != -1) {
            x2 = x1 + 1;
            x3 = x2 + 1;
            x4 = x3 + 1;
            y2 = y1;
            y3 = y1;
            y4 = y1;
          }
          if ( esquerda != -1) {
            x2 = x1 - 1;
            x3 = x2 - 1;
            x4 = x3 - 1;
            y2 = y1;
            y2 = y1;
            y3 = y2;
            y4 = y3;
          }
          if ( baixo != -1) {
            y2 = y1 + 1;
            y3 = y2 + 1;
            y4 = y3 + 1;
            x2 = x1;
            x3 = x2;
            x4 = x3;
          }
          if ( cima != -1) {
            y2 = y1 - 1;
            y3 = y2 - 1;
            y4 = y3 - 1;
            x2 = x1;
            x3 = x2;
            x4 = x3;
          }
          if (aviao != -1) {
            i++;
            a++;
            if(n==1 && i<=(numlinhas/2)){
            p1.getMapa().setportaavioes(a, x1, y1, x2, y2, x3, y3, x4, y4);
           }
            if(n==2 && i>(numlinhas/2)){
              p2.getMapa().setportaavioes(a, x1, y1, x2, y2, x3, y3, x4, y4);
            }

            valor++;
          }
          if (canoa != -1) {
            i++;
            c++;
            if(n==1 && i<=(numlinhas/2)){
               p1.getMapa().setcanoa(c,x1,y1);
             }
            if(n==2 && i>(numlinhas/2)){
              p2.getMapa().setcanoa(c,x1,y1);

            }
          }
          if (submarino != -1) {
            i++;
            x++;
            if(n==1 && i<=(numlinhas/2) ){
               p1.getMapa().setsubmarino(x , x1, y1,x2, y2);

             }
            if(n==2 && i>(numlinhas/2)){
               p2.getMapa().setsubmarino(x , x1, y1,x2, y2);

              }
        }


         else break;
      }
      // Input: lado[23]=3 e linha[23]=4 coluna[23]=0  Output: O lado do barco número 5 é para esquerda e tem coordenadas  com linha 4 e coluna 0
      //cout << line << endl;

    }
    //cout << a << " avioes " << c << " canoas e " << x << " submarinos" << endl;
    // a número de avioes, c número de canoas e s números de submarinos
  }
  else cout << "Unable to open file" << endl;


}
