#include "canoa.hpp"
#include <iostream>

Canoa::Canoa(){
  set_vida(1);
}
Canoa::~Canoa(){
}

int Canoa::get_vida(){
  return vida;
}
void Canoa::set_vida(int vida){
  this->vida = vida;
}
int Canoa::get_numero(){
  return numero;
}
void Canoa::set_numero(int numero){
  this->numero = numero;
}
void Canoa::habilidade(){
    return habilidade();
}
void Canoa::afundar(){
  cout << "A canoa foi afundada" << endl;
}
void Canoa::atacar(){
  cout << "A canoa foi atacada" << endl;
}
void Canoa::imprimir_dados(){
  cout<< "Canoa tem tamanho 1, e não possui habilidade" << endl;
  return imprimir_dados();
}
