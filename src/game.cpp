#include "batalhanaval.hpp"
Game::Game(){

}
Game::~Game(){
}

void Game::run(){
  int i = 0;
  string resposta = "";
  string nome = "";
  cout << "Digite o seu nome jogador número 1: " << endl;
  cin >> nome;
  p1.set_nome(nome);
  cout << "Você quer randomizar o mapa? (s/n)" << endl;
  cin >> resposta;
  if(resposta == "s"){
    coordenada.randomizarp1();
  }
  if(resposta == "n"){
    coordenada.colocarbarcos(1);
  }
  cout << "Você quer jogar contra a CPU? (s/n)" << endl;
  cin >> resposta;
  if(resposta == "s"){
    cout << "Escolha o nível de dificulade:" << endl << "1. Ataca aleatoriamente " << endl << "2. Ataca com alguma estrategia (se tiver um barco destruido ele vai no barco ao lado)" << endl << "3. GLaDOS (sabe onde estão as suas embarcações)" << endl;
    cin >> cpu;
    coordenada.randomizarp2();
  }
  if(resposta == "n"){
  cpu = 0;
  cout << "Digite o seu nome jogador número 2: " << endl;
  cin >> nome;
  p2.set_nome(nome);
  cout << "Você quer randomizar o mapa? (s/n)" << endl;
  cin >> resposta;
  if(resposta == "s"){
    coordenada.randomizarp2();
  }
  if(resposta == "n"){
    coordenada.colocarbarcos(2);
  }
}
while(true && cpu==3){
  int x=0;
  int y=0;
  cout << p1.get_nome() << " Digite onde você quer atacar " << endl;
  cin >> x >> y;
  coordenada.atacarbarcos_p2(x, y);

  cout << p1.get_nome();
  coordenada.pontuarp2();
  cout << " em cima da GLaDOS " << endl;


  cout << "TABELA GLaDOS: " << endl;
  coordenada.printarcampo_p2();

  coordenada.estrategiafinal(); // Sabe onde os barcos estão

  cout << "GLaDOS ";
  coordenada.pontuarp1();
  cout << " em cima do " << p1.get_nome() << endl;


  cout << "TABELA JOGADOR : " << p1.get_nome() << endl;
  coordenada.printarcampo_p1();
  i++;


  if(coordenada.checar_vitoriap2() == 0 && coordenada.checar_vitoriap1() == 0){
    cout << "GLaDOS VENCEU " << endl;
    return;
  }
  else if(coordenada.checar_vitoriap2() == 0){
    cout << "Vitoria!" << endl <<  "GLaDOS venceu" << endl;
    return;
  }
  else if(    coordenada.checar_vitoriap1() == 0){
    cout << "Vitoria!" << endl <<  "GLaDOS venceu" << endl;
    return;
  }

}
while(true && cpu==2){
  int x=0;
  int y=0;
  cout << p1.get_nome() << " Digite onde você quer atacar a CPU" << endl;
  cin >> x >> y;
  coordenada.atacarbarcos_p2(x, y);

  cout << p1.get_nome();
  coordenada.pontuarp2();
  cout << " em cima da CPU " << endl;


  cout << "TABELA CPU: " << endl;
  coordenada.printarcampo_p2();

  coordenada.estrategia();

  cout << "CPU ";
  coordenada.pontuarp1();
  cout << " em cima do " << p1.get_nome() << endl;


  cout << "TABELA JOGADOR : " << p1.get_nome() << endl;
  coordenada.printarcampo_p1();
  i++;


  if(coordenada.checar_vitoriap2() == 0 && coordenada.checar_vitoriap1() == 0){
    cout << "Empate! " << endl;
    return;
  }
  else if(coordenada.checar_vitoriap2() == 0){
    cout << "VITORIA!  " << p1.get_nome() << endl << " derrotou a CPU" << endl;
    return;
  }
  else if(    coordenada.checar_vitoriap1() == 0){
    cout << "CPU venceu" << endl;
    return;
  }

}
//Jogando contra cpu no nível 1
while(true && cpu==1){
    cout << p1.get_nome() << " possui essas embarcações: " << endl;
    coordenada.barcosrestantesp1();
    cout <<"A CPU possui essas embarcações " << endl;
    coordenada.barcosrestantesp2();

    int x=0;
    int y=0;
    cout << p1.get_nome() << " Digite onde você quer atacar a CPU" << endl;
    cin >> x >> y;
    coordenada.atacarbarcos_p2(x, y);

    cout << p1.get_nome();
    coordenada.pontuarp2();
    cout << " em cima da CPU " << endl;


    cout << "TABELA CPU: " << endl;
    coordenada.printarcampo_p2();

    coordenada.ataque_randomizado();

    cout << "CPU ";
    coordenada.pontuarp1();
    cout << " em cima do " << p1.get_nome() << endl;


    cout << "TABELA JOGADOR : " << p1.get_nome() << endl;
    coordenada.printarcampo_p1();
    i++;


    if(coordenada.checar_vitoriap2() == 0 && coordenada.checar_vitoriap1() == 0){
      cout << "Empate!!! " << endl;
      return;
    }
    else if(    coordenada.checar_vitoriap2() == 0){
      cout << "Vitoria !!!!! " << p1.get_nome() << " venceu" << endl;
      return;
    }
    else if(    coordenada.checar_vitoriap1() == 0){
      cout << "Vitoria !!!!! CPU venceu" << endl;
      return;
    }
}

while(true && cpu==0){
    cout << p1.get_nome() << " possui essas embarcações: " << endl;
    coordenada.barcosrestantesp1();
    cout << p2.get_nome() << " possui essas embarcações " << endl;
    coordenada.barcosrestantesp2();

    int x=0;
    int y=0;
    cout << p1.get_nome() << " Digite onde você quer atacar "<< p2.get_nome() << endl;
    cin >> x >> y;
    coordenada.atacarbarcos_p2(x, y);

    cout << p1.get_nome();
    coordenada.pontuarp2();
    cout << " em cima do " << p2.get_nome() << endl << endl;


    cout << "TABELA JOGADOR : " << p2.get_nome() << endl;
    coordenada.printarcampo_p2();

    cout << p2.get_nome() <<" Digite onde você quer atacar " << p1.get_nome() << endl;
    cin >> x >> y;
    coordenada.atacarbarcos_p1(x, y);

    cout << p2.get_nome();
    coordenada.pontuarp1();
    cout << " em cima do " << p1.get_nome() << endl;


    cout << "TABELA JOGADOR : " << p1.get_nome() << endl;
    coordenada.printarcampo_p1();
    i++;


    if(coordenada.checar_vitoriap2() == 0 && coordenada.checar_vitoriap1() == 0){
      cout << "Empate!!! " << endl;
      return;
    }
    else if(    coordenada.checar_vitoriap2() == 0){
      cout << "Vitoria !!!!! " << p1.get_nome() << " venceu" << endl;
      return;
    }
    else if(    coordenada.checar_vitoriap1() == 0){
      cout << "Vitoria !!!!! " << p2.get_nome() << " venceu" << endl;
      return;
    }

  }
}
