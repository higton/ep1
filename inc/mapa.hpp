#ifndef MAPA_HPP
#define MAPA_HPP
#include "submarino.hpp"
#include "pontuacao.hpp"
#include "portaAvioes.hpp"
#include <vector>
using namespace std;

class Mapa{
  private:
    Canoa coordenadas_canoa[13][13];
    Submarino coordenadas_submarino[13][13];
    PortaAvioes coordenadas_aviao[13][13];
    int matriz[13][13];
    Canoa canoa;
    Submarino submarino;
    PortaAvioes aviao;
    Pontuacao pontuacao;
    int x=0;
    int y=0;
  public:
    Mapa();
    ~Mapa();
    int q =1;

    vector<vector<Barco>> coordenada;


    void setbarco(int x1, int y1);
    std::vector<std::vector<Barco*>> p;  // 2D array em um vetor.

    Mapa &getcoordenadas();

    void estrategiafinal();
    void estrategia();
    void ataque_randomizado();
    int lugar_ocupado(int x,int y);
    int checar_vitoria();
    bool cabe_no_mapa(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
    void pontuar();
    void randomizar();
    void atacar(int x1, int y1);
    void numerocanoa(int c);
    void testar();
    void printarcampo();
    void barcosrestantes();
    void setcanoa(int c, int x1, int y1);
    void setsubmarino(int x, int x1,int y1, int x2, int y2);
    void setportaavioes(int a, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
};

#endif
