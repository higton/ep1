#ifndef PONTUACAO_HPP
#define PONTUACAO_HPP

using namespace std;

class Pontuacao{
  private:
  int pontos = 0;
  int status = -1;

  public:
    Pontuacao();
    ~Pontuacao();

  int get_pontos();
  void set_pontos(int pontos);

  int get_status();
  void set_status(int status);

};
#endif
