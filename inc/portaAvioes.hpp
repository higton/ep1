#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP

#include "barco.hpp"
using namespace std;

class PortaAvioes : public Barco{
  private:
    int vida;
    int a;
    int numero=0;
    int aviaonumero=0;
    public:
      PortaAvioes();
      ~PortaAvioes();

      void setaviaonumero(int n) { a = n; }
      int getaviaonumero() { return a; };
      // Sobreposição de Método
      void habilidade();
      void atacar();
      void imprimir_dados();
      void afundar();

      // Métodos acessores
      int get_vida();
      void set_vida(int vida);
      int get_numero();
      void set_numero(int aviaonumero);
};

#endif
