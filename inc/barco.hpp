#ifndef BARCO_HPP
#define BARCO_HPP

#include <string>

using namespace std;

class Barco {
  private:
  int tamanho;
  public:
    Barco();
    ~Barco();
    //Sobreposição
    virtual void habilidade() = 0;
    virtual void imprimir_dados() = 0;
    virtual void atacar() = 0;
    virtual void afundar() = 0;
    
    int get_tamanho();
    void set_tamanho(int tamanho);

};

#endif
