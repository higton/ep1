#ifndef CANOA_HPP
#define CANOA_HPP

#include "barco.hpp"
using namespace std;

class Canoa : public Barco{
private:
  int vida;
  int c;
  int canoanumero=0;
  int numero=0;
public:
    Canoa();
    ~Canoa();

    void setcanoanumero(int n) { c = n; }
    int getcanoanumero() { return c; };

    //Sobreposição
    void habilidade();
    void imprimir_dados();
    void atacar();
    void afundar();

    // Métodos acessores
    int get_vida();
    void set_vida(int vida);
    int get_numero();
    void set_numero(int canoanumero);
};
#endif
