#ifndef JOGADOR_HPP
#define JOGADOR_HPP
#include "mapa.hpp"
using namespace std;

class Jogador{
  private:
    string nome;
    string nacionalidade;
    Mapa tabelajogador;
  public:
    Jogador();
    ~Jogador();
    int q =1;
    void apresentar();
    void status();
    void ganhar();
    void perder();
    Mapa& getMapa() { return tabelajogador; }





    //Métodos acessores
    string get_nome();
    void set_nome(string nome);

    string get_nacionalidade();
    void set_nacionalidade(string nome);


};

#endif
