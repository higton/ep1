#ifndef GAME_HPP
#define GAME_HPP

using namespace std;

class Game{
  private:
  Coordenadas coordenada;
  Jogador p1;
  Jogador p2;
  Pontuacao pontuacao;
  int cpu=0;
  public:
    Game();
    ~Game();

    void run();
};
#endif
