#ifndef COORDENADAS_HPP
#define COORDENADAS_HPP

using namespace std;

class Coordenadas{
  private:
    int listadebarcos;
    Jogador p1;
    Jogador p2;
    Pontuacao pontuacao;
    int numlinhas;
    Mapa mapa;
    int n = 0;
  public:
    Coordenadas();
    ~Coordenadas();

    void estrategiafinal();
    void estrategia();
    void ataque_randomizado();
    void pontuarp1();
    void pontuarp2();
    void barcosrestantesp1();
    void barcosrestantesp2();
    int checar_vitoriap1();
    int checar_vitoriap2();
    void colocarbarcos(int n);
    void atacarbarcos_p1(int x1, int y1);
    void atacarbarcos_p2(int x1, int y1);
    void printarcampo_p1();
    void printarcampo_p2();
    void randomizarp1();
    void randomizarp2();

    //Métodos acessores
    int get_numlinhas();
    void set_numlinhas(int numlinhas);


    int get_listadebarcos();
    void set_listadebarcos(int listadebarcos);
};
#endif
