#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "barco.hpp"
using namespace std;

class Submarino : public Barco{
private:
    int vida;
    int c;
    int numero=0;
    int submarinonumero=0;
  public:
    Submarino();
    ~Submarino();

    void setsubmarinonumero(int n) { c = n; }
    int getsubmarinonumero() { return c; };

    //Sobreposição de métodos
    void habilidade();
    void atacar();
    void imprimir_dados();
    void afundar();

    // Métodos acessores
    int get_vida();
    void set_vida(int vida);

    int get_numero();
    void set_numero(int submarinonumero);
};

#endif
